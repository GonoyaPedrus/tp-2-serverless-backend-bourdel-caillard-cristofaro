#First we will create our aws_dynamodb_table,you can find the name in the variables.tf, the billing_mode will be PROVISIONED with a read_capacity and a write_capacity of 5,the hash_key will be id and the range_key city, both of them will need to be defined as attributes of type S
resource aws_dynamodb_table "job-table"{
    name = var.dynamo_job_table_name
    billing_mode = "PROVISIONED"
    read_capacity = 5
    write_capacity = 5
    hash_key = "id"
    range_key = "city"
    attribute {
        name = "id"
        type = "S"
    }
    attribute {
        name = "city"
        type = "S"
    }
}
