resource aws_s3_bucket "s3_job_offer_bucket_bourdel_caillard_cristofaro"{
  bucket = "s3-job-offer-bucket-bourdel-caillard-cristofaro"
}

resource aws_s3_object "job_offers"{
  bucket = aws_s3_bucket.s3_job_offer_bucket_bourdel_caillard_cristofaro.id
  key = "job_offers/"
}

resource aws_s3_bucket_notification "bucket_notification"{
  bucket = aws_s3_bucket.s3_job_offer_bucket_bourdel_caillard_cristofaro.id

    lambda_function {
        lambda_function_arn = aws_lambda_function.s3_to_sqs_lambda.arn
        events = ["s3:ObjectCreated:*"]
        filter_prefix = "job_offers/"
        filter_suffix = ".csv"
    }
}

